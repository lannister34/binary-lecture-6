import React, {Component} from "react";
import Chat from "./containers/Chat";
import Header from './page/Header/Header';
import Footer from "./page/Footer/Footer";
import {AppProps, AppState} from "./interfaces/AppInterface";

class App extends Component<AppProps, AppState> {
    constructor(props: AppProps) {
        super(props);
        this.state = {
            chatId: "1",
            chatName: "My chat",
            currentUserId: "80e03248-1b8f-11e8-9629-c7eca82aa7bd"
        }
    }

    render() {
        const header: JSX.Element = <Header />;
        const chat: JSX.Element = <Chat chatId={this.state.chatId} chatName={this.state.chatName} currentUserId={this.state.currentUserId} />
        const footer: JSX.Element = <Footer />;

        const DOMFragment: Array<JSX.Element> = [header, chat, footer];
        return (
            DOMFragment
        );
    }
}

export default App;
