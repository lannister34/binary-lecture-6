import React, {Component} from "react";
import Header from "./Header";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import LoadingSpinner from "../components/LoadingSpinner";
import {ChatProps, ChatState} from "../interfaces/ChatInterface";
import {MessageProps} from "../interfaces/MessageInterface";

import "../styles/chat.css";

class Chat extends Component<ChatProps, ChatState> {
    private API_LINK = "https://api.npoint.io/b919cb46edac4c74d0a8";

    constructor(props: ChatProps) {
        super(props);

        this.state = {
            userCount: 23,
            messageList: [],
            messageCount: 0,
            lastMessageTime: null,
            isGotData: false
        };
    }

    async componentDidMount() {
        const messageList: Array<MessageProps> = await this.fetchMessages();
        const sortedMessageList: Array<MessageProps> = this.sortMessages(messageList);

        this.setState(() => {
            return {
                messageList: sortedMessageList,
                messageCount: messageList.length
            }
        });

        this.setState(() => {
            return {
                lastMessageTime: this.lastMessageTime,
                isGotData: true
            }
        });
    }

    async fetchMessages() {
        return fetch(this.API_LINK + "?id=" + this.props.chatId)
            .then((response: Response) => (response.ok ? response.json() : []))
    }

    sortMessages(messageList: Array<MessageProps>) {
        return messageList.sort((prev: MessageProps, curr: MessageProps) => {
            const prevDate: number = new Date(prev.createdAt).getTime();
            const currDate: number = new Date(curr.createdAt).getTime();
            return prevDate - currDate;
        })
    }

    get lastMessageTime() {
        const lastMessageIndex: number = this.state.messageList.length - 1;
        if (this.state.messageList[lastMessageIndex]) {
            const date: Date = new Date(this.state.messageList[lastMessageIndex].createdAt);
            return String(date.getHours()) + ":" + String(date.getMinutes());
        }
        return null;
    }

    render() {
        let content: JSX.Element;

        if (this.state.isGotData) {
            content = (
                <div className="chat__wrapper container" key={this.props.chatId}>
                    <Header id={this.props.chatId} name={this.props.chatName} userCount={this.state.userCount}
                            messageCount={this.state.messageCount} lastMessageTime={this.state.lastMessageTime}/>
                    <MessageList messageList={this.state.messageList} currentUserId={this.props.currentUserId} />
                    <MessageInput/>
                </div>
            );
        } else {
            content = <LoadingSpinner/>
        }

        return (
            content
        );
    }
}


export default Chat;
